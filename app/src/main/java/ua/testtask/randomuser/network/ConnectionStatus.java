package ua.testtask.randomuser.network;

public interface ConnectionStatus {

    interface Callback {

        void onConnectionRestored();

    }

    boolean isConnected();

    void cleanUp();

}
