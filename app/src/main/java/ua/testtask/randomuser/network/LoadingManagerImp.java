package ua.testtask.randomuser.network;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ua.testtask.randomuser.data.ServerResponseData;

public class LoadingManagerImp implements LoadingManager {

    private final RandomUserApi mRandomUseApi;
    private int mCurrentPage = LoadingManager.PAGE_START;
    private boolean mIsLoading = false;
    private boolean mIsLastPage = false;

    public LoadingManagerImp() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(LoadingManager.RANDOM_USER_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mRandomUseApi = retrofit.create(RandomUserApi.class);
    }

    @Override
    public void requestData(LoadingManagerCallback callback) {
        mIsLoading = true;
        mCurrentPage++;
        mRandomUseApi.getData(mCurrentPage, LoadingManager.NUMBER_OF_RESULTS, LoadingManager.INCLUDE_ONLY).enqueue(new Callback<ServerResponseData>() {
            @Override
            public void onResponse(Call<ServerResponseData> call, Response<ServerResponseData> response) {
                if (response.body() != null) {
                    mIsLoading = false;
                    if (mCurrentPage == LoadingManager.TOTAL_PAGES) mIsLastPage = true;
                    callback.onReceived(response.body().getUserData());
                    if (mCurrentPage > LoadingManager.TOTAL_PAGES) mIsLastPage = true;
                }
            }

            @Override
            public void onFailure(Call<ServerResponseData> call, Throwable t) {
                callback.onFailure();
            }
        });
    }

    @Override
    public boolean isLastPage() {
        return mIsLastPage;
    }

    @Override
    public boolean isLoading() {
        return mIsLoading;
    }

}
