package ua.testtask.randomuser.network;

import java.util.List;

import ua.testtask.randomuser.data.UserData;


public interface LoadingManager {

    interface LoadingManagerCallback {

        void onReceived(List<UserData> data);

        void onFailure();

    }

    String RANDOM_USER_BASE_URL = "https://randomuser.me/";
    String INCLUDE_ONLY = "name,gender,dob,thumbnail,large,phone,cell,picture,location,registered,login,email";
    int TOTAL_PAGES = 10;
    int NUMBER_OF_RESULTS = 20;
    int PAGE_START = 0;

    void requestData(LoadingManagerCallback callback);

    boolean isLastPage();

    boolean isLoading();

}
