package ua.testtask.randomuser.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ua.testtask.randomuser.data.ServerResponseData;

public interface RandomUserApi {

    @GET("api")
    Call<ServerResponseData> getData(@Query("page") int pageIndex, @Query("results") int numberOfResult,
                                     @Query("inc") String includeOnly);

}
