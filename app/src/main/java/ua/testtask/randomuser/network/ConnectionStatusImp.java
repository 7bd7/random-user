package ua.testtask.randomuser.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionStatusImp extends BroadcastReceiver implements ConnectionStatus {

    private final ConnectivityManager mConnectivityManager;
    private final Callback mCallback;
    private final Context mContext;

    private boolean mIsConnected;

    public ConnectionStatusImp(Context context, Callback callback) {
        mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        mCallback = callback;
        mContext = context;
        mContext.registerReceiver(this, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        isConnected();
    }

    @Override
    public boolean isConnected() {
        NetworkInfo activeNetwork = mConnectivityManager.getActiveNetworkInfo();
        mIsConnected = activeNetwork != null && activeNetwork.isConnected();
        return mIsConnected;
    }

    @Override
    public void cleanUp() {
        mContext.unregisterReceiver(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent == null || intent.getExtras() == null)
            return;
        if (!mIsConnected && isConnected()) mCallback.onConnectionRestored();
    }

}
