package ua.testtask.randomuser;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Locale;

import ua.testtask.randomuser.data.Coordinates;
import ua.testtask.randomuser.data.UserData;
import ua.testtask.randomuser.util.CircleTransform;

public class ProfileActivity extends AppCompatActivity {

    public static Intent from(Context context, UserData data) {
        return new Intent(context, ProfileActivity.class)
                .putExtra(EXTRA_DATA, data);
    }

    private static final String EXTRA_DATA = "EXTRA_DATA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_profile);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Intent extras data surely set in HomeActivity
        //noinspection ConstantConditions
        UserData userData = (UserData) getIntent().getExtras().getSerializable(EXTRA_DATA);
        //noinspection ConstantConditions
        bindView(R.id.a_profile_name, userData.getName().getFullName(), null);
        bindView(R.id.a_profile_age, getResources().getQuantityString(R.plurals.years, userData.getAgeInfo().getAge(),
                userData.getAgeInfo().getAge()), null);
        bindView(R.id.a_profile_phone, userData.getPhone(), () -> safeStart(call(userData.getPhone())));
        Spannable cell = new SpannableString( userData.getCellPhone() + " (" + getString(R.string.cellphone) + ")");
        cell.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,
                R.color.colorSecondary)), userData.getCellPhone().length(), cell.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        bindView(R.id.a_profile_cell, cell, () -> safeStart(call(userData.getCellPhone())));
        bindView(R.id.a_profile_email, userData.getEmail(), () -> safeStart(composeEmail(userData.getEmail())));
        bindView(R.id.a_profile_birth_date, formatDate(userData.getAgeInfo().getDateOfBirth()),
                () -> safeStart(addToCalendar(userData.getAgeInfo().getDateOfBirth(), userData.getName().getFullName())));
        bindView(R.id.a_profile_location, userData.getLocation().getState() + ", " + userData.getLocation().getCity(),
                () -> safeStart(openInMap(userData.getLocation().getCoordinates())));
        bindView(R.id.a_profile_user_name, userData.getLoginInfo().getUsername(), null);
        bindView(R.id.a_profile_registration_date, getString(R.string.registered) + " " + formatDate(userData.getRegisterInfo().getDate()), null);
        findViewById(R.id.a_profile_user_name_view).setOnClickListener(v -> copyToClipboard(userData.getLoginInfo().getUsername()));

        ((TextView) findViewById(R.id.a_profile_name)).setText(userData.getName().getFullName());
        Picasso.get()
                .load(Uri.parse(userData.getPicture().getLarge()))
                .placeholder(R.drawable.ic_person)
                .transform(new CircleTransform())
                .into((ImageView) findViewById(R.id.a_profile_photo));
        ((ImageView) findViewById(R.id.a_profile_gender))
                .setImageResource(TextUtils.equals(userData.getGender(), UserData.GENDER_MALE)
                        ? R.drawable.ic_male : R.drawable.ic_female);
    }

    private void bindView(int resourceId, CharSequence data, @Nullable Runnable onClick) {
        TextView view = findViewById(resourceId);
        view.setText(data);
        if (onClick != null) view.setOnClickListener(v -> onClick.run());
    }

    private Intent call(String number) {
        return new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null));
    }

    private Intent composeEmail(String address) {
        return new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", address, null));
    }

    private Intent addToCalendar(String dateOfBirth, String name) {
        return new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.Events.TITLE, getString(R.string.birthday_event_title) + " " + name)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, parseDate(dateOfBirth));
    }

    private long parseDate(String YYYY_MM_DD_HH_MM_SS_) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.valueOf(YYYY_MM_DD_HH_MM_SS_.substring(0, 4)));
        calendar.set(Calendar.MONTH, Integer.valueOf(YYYY_MM_DD_HH_MM_SS_.substring(5,7)) - 1); // -1 because Calendar.JANUARY = 0;
        calendar.set(Calendar.DATE, Integer.valueOf(YYYY_MM_DD_HH_MM_SS_.substring(8,10)));
        return calendar.getTimeInMillis();
    }

    private Intent openInMap(Coordinates coordinates) {
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f",
                Float.parseFloat(coordinates.getLatitude()), Float.parseFloat(coordinates.getLongitude()));
        return new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
    }

    private String formatDate(String YYYY_MM_DD_HH_MM_SS_) {
        return YYYY_MM_DD_HH_MM_SS_.substring(0, 10);
    }

    private void copyToClipboard(String text) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboard != null) {
            clipboard.setPrimaryClip(ClipData.newPlainText("randomUser", text));
            Toast.makeText(this, R.string.copied_to_clipboard, Toast.LENGTH_SHORT).show();
        }
    }

    private void safeStart(Intent intent) {
        if (intent == null || intent.resolveActivity(getPackageManager()) == null) {
            Snackbar.make(findViewById(android.R.id.content), R.string.no_app, Snackbar.LENGTH_LONG).show();
        } else startActivity(intent);
    }

}
