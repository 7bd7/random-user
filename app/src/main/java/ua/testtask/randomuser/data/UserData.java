
package ua.testtask.randomuser.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserData implements Serializable {

    public static final String GENDER_MALE = "male";

    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("name")
    @Expose
    private Name name;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("login")
    @Expose
    private LoginInfo loginInfo;
    @SerializedName("dob")
    @Expose
    private AgeInfo ageInfo;
    @SerializedName("registered")
    @Expose
    private RegisterInfo registerInfo;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("cell")
    @Expose
    private String cellPhone;
    @SerializedName("picture")
    @Expose
    private Picture picture;

    public String getGender() {
        return gender;
    }

    public Name getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public String getEmail() {
        return email;
    }

    public LoginInfo getLoginInfo() {
        return loginInfo;
    }

    public AgeInfo getAgeInfo() {
        return ageInfo;
    }

    public RegisterInfo getRegisterInfo() {
        return registerInfo;
    }

    public String getPhone() {
        return phone;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public Picture getPicture() {
        return picture;
    }

}
