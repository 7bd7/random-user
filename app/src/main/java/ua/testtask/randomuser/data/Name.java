
package ua.testtask.randomuser.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ua.testtask.randomuser.util.Utils;


public class Name implements Serializable {

    @SerializedName("first")
    @Expose
    private String first;
    @SerializedName("last")
    @Expose
    private String last;

    public String getFullName() {
        return Utils.capitalize(first) + " " + Utils.capitalize(last);
    }

}
