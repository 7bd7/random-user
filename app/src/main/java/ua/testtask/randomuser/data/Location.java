
package ua.testtask.randomuser.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ua.testtask.randomuser.util.Utils;

public class Location implements Serializable {

    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("coordinates")
    @Expose
    private Coordinates coordinates;

    public String getCity() {
        return Utils.capitalize(city);
    }

    public String getState() {
        return Utils.capitalize(state);
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

}
