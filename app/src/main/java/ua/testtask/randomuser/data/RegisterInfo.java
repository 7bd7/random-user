
package ua.testtask.randomuser.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RegisterInfo implements Serializable {

    @SerializedName("date")
    @Expose
    private String date;

    public String getDate() {
        return date;
    }

}
