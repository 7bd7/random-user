
package ua.testtask.randomuser.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServerResponseData {

    @SerializedName("results")
    @Expose
    private List<UserData> userData;

    public List<UserData> getUserData() {
        return userData;
    }

}
