
package ua.testtask.randomuser.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginInfo implements Serializable {

    @SerializedName("username")
    @Expose
    private String username;

    public String getUsername() {
        return username;
    }

}
