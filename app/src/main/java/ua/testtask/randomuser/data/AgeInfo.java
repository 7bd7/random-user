
package ua.testtask.randomuser.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AgeInfo implements Serializable {

    @SerializedName("date")
    @Expose
    private String dateOfBirth;
    @SerializedName("age")
    @Expose
    private Integer age;

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public Integer getAge() {
        return age;
    }

}
