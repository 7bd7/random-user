package ua.testtask.randomuser.home;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ua.testtask.randomuser.R;
import ua.testtask.randomuser.data.UserData;
import ua.testtask.randomuser.util.CircleTransform;

class RandomUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface Callback {

        void onItemClicked(UserData data);

    }

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private static final UserData FOOTER_FAKE_USER_DATA = new UserData();

    private final Callback mCallback;
    private boolean isLoadingAdded = false;
    private List<UserData> mData;

    RandomUserAdapter(List<UserData> data, Callback callback) {
        mData = data;
        mCallback = callback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return viewType == ITEM ? new UserVh(parent) : new LoadingVh(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        UserData data = mData.get(position);
        if (getItemViewType(position) == ITEM)
            ((UserVh) viewHolder).bind(Uri.parse(data.getPicture().getThumbnail()),
                    data.getName().getFullName(), data.getLocation().getState() + ", "
                            + viewHolder.itemView.getContext().getResources()
                            .getQuantityString(R.plurals.years, data.getAgeInfo().getAge(), data.getAgeInfo().getAge()));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mData.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void setData(List<UserData> data) {
        mData = data;
        notifyDataSetChanged();
    }

    private void add(UserData data) {
        mData.add(data);
        notifyItemInserted(mData.size() - 1);
    }

    void addAll(List<UserData> list) {
        for (UserData userData : list) add(userData);
    }

    void addLoadingFooter() {
        isLoadingAdded = true;
        add(FOOTER_FAKE_USER_DATA);
    }

    void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mData.size() - 1;
        UserData item = mData.get(position);

        if (item != null) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    class UserVh extends RecyclerView.ViewHolder {

        private final ImageView mPhoto;
        private final TextView mName;
        private final TextView mCountryAge;

        UserVh(@NonNull ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.i_user, parent, false));
            mPhoto = itemView.findViewById(R.id.i_user_thumbnail);
            mName = itemView.findViewById(R.id.i_user_name);
            mCountryAge = itemView.findViewById(R.id.i_user_country_age);
            itemView.setOnClickListener(v -> {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    mCallback.onItemClicked(mData.get(getAdapterPosition()));
                }
            });
        }

        void bind(Uri photoUri, String name, String countryAge) {
            mName.setText(name);
            mCountryAge.setText(countryAge);
            Picasso.get()
                    .load(photoUri)
                    .placeholder(R.drawable.ic_person)
                    .transform(new CircleTransform())
                    .into(mPhoto);
        }

    }

    class LoadingVh extends RecyclerView.ViewHolder {

        LoadingVh(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.i_loading, parent, false));
        }

    }

}
