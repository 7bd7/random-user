package ua.testtask.randomuser.util;

import android.text.TextUtils;

public class Utils {

    public static String capitalize(String s) {
        if (TextUtils.isEmpty(s)) return s;
        char c[] = s.toCharArray();
        c[0] = Character.toUpperCase(c[0]);
        return new String(c);
    }

}
