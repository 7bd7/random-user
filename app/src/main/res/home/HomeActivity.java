package ua.testassignment.randomuser.home;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Collections;
import java.util.List;

import ua.testassignment.randomuser.R;
import ua.testassignment.randomuser.data.UserData;
import ua.testassignment.randomuser.network.ConnectionStatus;
import ua.testassignment.randomuser.network.ConnectionStatusImp;
import ua.testassignment.randomuser.network.LoadingManager;
import ua.testassignment.randomuser.network.LoadingManagerImp;
import ua.testassignment.randomuser.profile.ProfileActivity;

public class HomeActivity extends AppCompatActivity {

    private View mLoadingView;
    private View mNoInternetView;
    private RecyclerView mRv;
    private RandomUserAdapter mAdapter;
    private ConnectionStatus mConnectionStateManager;
    private LoadingManagerImp mLoadingManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_home);

        // Init components
        mConnectionStateManager = new ConnectionStatusImp(this,
                () -> {
                    // When connection restored
                    loadData(true);
                    updateViewsVisibility(true);
                    Snackbar.make(mLoadingView, R.string.connection_restored, Snackbar.LENGTH_LONG)
                            .show();
                });
        mLoadingManager = new LoadingManagerImp();

        initViews();
        if (mConnectionStateManager.isConnected()) loadData(true);
        updateViewsVisibility(mConnectionStateManager.isConnected());
    }

    private void initViews() {
        mLoadingView = findViewById(R.id.a_home_loading);
        mNoInternetView = findViewById(R.id.a_home_no_internet);
        findViewById(R.id.a_home_reconnect).setOnClickListener(v -> {
            if (mConnectionStateManager.isConnected()) {
                loadData(true);
                updateViewsVisibility(mConnectionStateManager.isConnected());
            } else Snackbar.make(mLoadingView, R.string.no_connection, Snackbar.LENGTH_SHORT)
                    .show();
        });
        initRv();
    }

    private void initRv() {
        mRv = findViewById(R.id.a_home_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRv.setLayoutManager(layoutManager);
        mAdapter = new RandomUserAdapter(Collections.emptyList(),
                data -> startActivity(ProfileActivity.from(this, data)));
        mRv.setItemAnimator(new DefaultItemAnimator());
        mRv.setAdapter(mAdapter);
        mRv.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            public void loadMoreItems() {
                loadData(false);
            }

            @Override
            public boolean isLastPage() {
                return mLoadingManager.isLastPage();
            }

            @Override
            public boolean isLoading() {
                return mLoadingManager.isLoading();
            }
        });
    }

    private void updateViewsVisibility(boolean connected) {
        mRv.setVisibility(connected ? View.VISIBLE : View.GONE);
        mNoInternetView.setVisibility(connected ? View.GONE : View.VISIBLE);
        mLoadingView.setVisibility(connected ? View.VISIBLE : View.GONE);
    }

    private void loadData(boolean isFirstTime) {
        mLoadingManager.requestData(new LoadingManager.LoadingManagerCallback() {
            @Override
            public void onReceived(List<UserData> data) {
                if (data.size() > 0) {
                    if (isFirstTime) {
                        mAdapter.setData(data);
                        // Hide loading progress bar
                        mLoadingView.setVisibility(View.GONE);
                    } else {
                        mAdapter.removeLoadingFooter();
                        mAdapter.addAll(data);
                    }
                    if (!mLoadingManager.isLastPage()) mAdapter.addLoadingFooter();
                } else showNoDataMessage(isFirstTime);
            }

            @Override
            public void onFailure() {
                showLoadingFail(isFirstTime);
            }
        });
    }

    private void showNoDataMessage(boolean isFirstTimeLoading) {
        Snackbar.make(mLoadingView, R.string.no_data, Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.reload), v -> loadData(isFirstTimeLoading))
                .show();
    }

    private void showLoadingFail(boolean isFirstTimeLoading) {
        Snackbar.make(mLoadingView, R.string.error_loading, Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.reload), v -> loadData(isFirstTimeLoading))
                .show();
    }

    @Override
    protected void onDestroy() {
        mConnectionStateManager.cleanUp();
        super.onDestroy();
    }

}
